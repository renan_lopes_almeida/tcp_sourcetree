﻿using System.Collections;
public interface IBlock
{
    bool Execute();
}
