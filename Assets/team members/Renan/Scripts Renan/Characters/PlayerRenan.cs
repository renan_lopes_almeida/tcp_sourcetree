﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerRenan : CharacterRenan
{
    

    private KeyCode keyPressed;

    [SerializeField]
    float powerJump;

    GameController gameController;

    protected override void Start()
    {
        base.Start();
        gameController = GameController.instance;
    }

    // Update is called once per frame
    void Update()
    {
        this.ListenKeys();
        this.Move();

        

    }

    protected override void LateUpdate()
    {
        base.LateUpdate();
    }

    public void Move()
    {
        this.distance = Vector3.Distance(transform.position, this.target);

        if (this.distance > 0)
        {
            this.isMoving = true;
            transform.position = Vector3.Lerp(transform.position, this.target, Time.deltaTime * this.speed / this.distance);
        }
        else
        {
            this.isMoving = false;
        }
    }

    public void ListenKeysRefactored(CharacterEnumRenan characterEnum)
    {
        if (!isMoving)
        {
            switch (characterEnum)
            {
                case CharacterEnumRenan.RIGHT:
                    this.target = new Vector3(transform.position.x + 5.2f, transform.position.y, transform.position.z);
                    break;

                case CharacterEnumRenan.LEFT:
                    this.target = new Vector3(transform.position.x - 5.2f, transform.position.y, transform.position.z);
                    break;

                case CharacterEnumRenan.FORWARD:
                    this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5.2f);
                    break;

                case CharacterEnumRenan.JUMP:
                    this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5.2f);
                    this.rigidBody.AddForce(new Vector3(0, powerJump, 0));
                    break;

                case CharacterEnumRenan.DOWN:
                    this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5.2f);
                    break;
            }
        }
    }

    void ListenKeys()
    {
        
        if (Input.GetKeyDown(KeyCode.LeftArrow) && !isMoving)
        {
            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(-5.2f, 0, 0), Time.deltaTime * 1f);
            this.target = new Vector3(transform.position.x - 5.2f, transform.position.y, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(5.2f, 0, 0));
            this.target = new Vector3(transform.position.x + 5.2f, transform.position.y, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(0, 0, 5.2f));
            this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5.2f);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(0, 0, -5.2f));
            this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5.2f);
        }
    }

    public bool  WriteDown()
    {
         print("aaa");

        return true;
    }

    //public void MoveRefactored(float distance)
    //{
    //    float tempDistance = distance;
    //    foreach(KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))){
    //         if(Input.GetKey(vKey)){
    //            this.keyPressed = vKey;

    //        }
    //     }

    //    if (keyPressed == KeyCode.LeftArrow || keyPressed == KeyCode.DownArrow)
    //        distance *= -1;

    //    else
    //        distance = tempDistance;

        
    //    if(keyPressed == KeyCode.LeftArrow || keyPressed == KeyCode.RightArrow)
    //        target = new Vector3(transform.position.x + distance, transform.position.y, transform.position.z);

    //    else   
    //        target = new Vector3(transform.position.x, transform.position.y, transform.position.y + distance);


    //}
}
