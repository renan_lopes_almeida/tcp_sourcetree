﻿using UnityEngine;
using System.Collections;

public enum CharacterEnumRenan
{
    IDLE,
    WALK,
    JUMP,
    LEFT,
    RIGHT,
    FORWARD,
    DOWN,
    ROTATE
}
