﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class CharacterRenan : MonoBehaviour
{
    private float distanceToWalk;
    protected float jumpForce;
    protected int life;
    protected Rigidbody rigidBody;
    protected CharacterEnumRenan characterEnum;
    protected Animator anim;  

    protected bool isMoving;
    protected Vector3 target;
    protected float speed;
    protected float distance;

    public CharacterRenan() { }

    public CharacterRenan(CharacterController c)
    {

    }

    protected virtual void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        this.target = transform.position;
        this.speed = 3f;
        this.distance = 0f;
        this.isMoving = false;
    }

    protected virtual void LateUpdate()
    {
        AnimManager();
    }

    void AnimManager()
    {
        switch (characterEnum)
        {
            case CharacterEnumRenan.IDLE:
                anim.SetBool("idle", true);
                anim.SetBool("walk", false);
                anim.SetBool("jump", false);
                break;

            case CharacterEnumRenan.WALK:
                anim.SetBool("walk", true);
                anim.SetBool("idle", false);
                anim.SetBool("jump", false);
                break;

            case CharacterEnumRenan.JUMP:
                anim.SetBool("jump", true);
                anim.SetBool("walk", false);
                anim.SetBool("idle", false);
                break;


        }
    }

    

    public void Jump(float jumpForce)
    {

    }

}
