﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance { get; set; }
    PlayerRenan player;
    CharacterRenan character;
    [SerializeField]
    float currentTime, maxTime = 1.5f;
    [SerializeField] int currentIndexTask;
    bool callTimer, executeTasks;

    [SerializeField]
    int currentIndex;

    public List<IBlock> blockActions { get; set; }
    public bool nextItem { get; set; }

    delegate bool MyDelegate();
    MyDelegate myDelegate;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    
    void Start()
    {
        blockActions = new List<IBlock>();
        player = FindObjectOfType(typeof(PlayerRenan)) as PlayerRenan;

    }

    
    void Update()
    {
        TimerToExecuteBlockTasks();
    }

    

    //void TimerToExecuteBlockTasks()
    //{

    //    if (callTimer)
    //    {
    //        //blockActions[currentIndexTask].Execute();
    //        currentTime += Time.deltaTime;
    //        if (currentTime >= maxTime)
    //        {
    //            if (currentIndexTask < blockActions.Count)
    //            {
    //                blockActions[currentIndexTask].Execute();
    //                currentIndexTask++;
    //                currentTime = 0;
    //            }
    //            else
    //            {
    //                currentIndexTask = blockActions.Count - 1;
    //                blockActions[currentIndexTask].Execute();
    //                callTimer = false;
    //            }



    //        }
    //    }


    //}

    void TimerToExecuteBlockTasks()
    {

        if (callTimer)
        {

            for (int i = 0; i < blockActions.Count; i++)
            {
                print("i = " + i);
                currentTime += Time.deltaTime;
                if (currentTime >= maxTime)
                {
                    if (blockActions[i].Execute())
                        myDelegate = blockActions[i].Execute;

                    myDelegate();

                    currentTime = 0;
                }

                if (i >= blockActions.Count)
                    callTimer = false;

            }
        }

            //if (currentIndexTask < blockActions.Count)
            //{
            //    blockActions[currentIndexTask].Execute();

            //    blockActions[currentIndexTask++].Execute();

            //    //currentTime += Time.deltaTime;
            //    //if (currentTime >= maxTime)
            //    //{


            //    //    currentTime = 0;
            //    //}



            //}
        


    }

    void IncreaseIndexTask()
    {
        print("dsadadada");
        blockActions[currentIndexTask].Execute();
        currentIndexTask++;
    }

    public void Executar()
    {
        callTimer = true;
    }


}
