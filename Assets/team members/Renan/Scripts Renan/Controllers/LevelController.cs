﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{

    public LevelController instance { get; set; }
    public int currentIndexScene { get; set; }
    
    
    void Awake()
    {
        if (instance == null)
            instance = new LevelController();

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    //public void RedirectToScene(int index)
    //{
    //    this.currentIndexScene = index;
    //    SceneManager.LoadScene(indexScene);
    //}

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
