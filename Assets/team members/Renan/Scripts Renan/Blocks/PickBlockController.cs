﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class PickBlockController : MonoBehaviour
{
    [SerializeField]
    private Transform blockPlace;
    private float deltaX, deltaY, deltaZ;
    private Vector3 initialPosition, mousePosition;
    private static bool locked;
    private bool startDrag;

    
    private RectTransform rectTransform;

    [Range(0.1f, 50)]
    public float speed = 41f;

    GameController gameController;


    void Start()
    {
        //initialPosition = new Vector2((float) RectTransform.Axis.Horizontal,(float) RectTransform.Axis.Vertical);
        rectTransform = GetComponent<RectTransform>();
        gameController = GameController.instance;
    }



    void OnMouseDown()
    {
        if (!locked)
        {
            deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
            deltaZ = Camera.main.ScreenToWorldPoint(Input.mousePosition).z - transform.position.z;
        }
    }

    private void OnMouseDrag()
    {
        if (!locked)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x - deltaX, mousePosition.y - deltaY, mousePosition.z - deltaZ);
        }
    }

    private void OnMouseUp()
    {
        //locked = true;
        gameController.blockActions.Add(GetComponent<IBlock>());
    }

    private void OnMouseOver()
    {
        //this.Update();
    }

    public static bool getLocked()
    {
        return locked;
    }
}
