﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMove : Block, IBlock
{
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Execute()
    {
        print("Move...");
        player.ListenKeysRefactored(CharacterEnumRenan.FORWARD);
        return true;
    }
}
