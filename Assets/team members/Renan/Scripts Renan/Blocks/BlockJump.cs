﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockJump : Block, IBlock
{
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Execute()
    {
        print("Jump...");
        this.player.ListenKeysRefactored(CharacterEnumRenan.JUMP);
        return true;

    }
}
