﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector3 target;
    public float speed;
    public float distance;
    public bool isMoving;

    void Start()
    {
        this.target = transform.position;
        this.speed = 3f;
        this.distance = 0f;
        this.isMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.Move();

        if (Input.GetKeyDown(KeyCode.LeftArrow) && ! isMoving)
        {
            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(-5.2f, 0, 0), Time.deltaTime * 1f);
            this.target = new Vector3( transform.position.x - 5.2f, transform.position.y, transform.position.z );
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(5.2f, 0, 0));
            this.target = new Vector3( transform.position.x + 5.2f, transform.position.y, transform.position.z );
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(0, 0, 5.2f));
            this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5.2f);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) && !isMoving)
        {
            // transform.Translate(transform.position + new Vector3(0, 0, -5.2f));
            this.target = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5.2f);
        }

        
    }

    

    public void Move()
    {
        this.distance = Vector3.Distance(transform.position, this.target);

        if (this.distance > 0)
        {
            this.isMoving = true;
            transform.position = Vector3.Lerp(transform.position, this.target, Time.deltaTime * this.speed / this.distance);
        }
        else
        {
            this.isMoving = false;
        }
    }
}
